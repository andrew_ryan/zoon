#[allow(warnings)]
pub mod state {
    use std::{
        borrow::BorrowMut,
        cell::RefCell,
    };

    /// State
    /// ```rust
    ///let a:State<i32> = State::new();    
    ///let b:State<i32> = State::new();    
    ///a.set_state(100);
    ///b.set_state(100);
    ///assert_eq!(a,b);
    ///&a.set_state(1000);
    ///assert_ne!(a.get_state(),b.get_state());
    /// ```
    #[derive(Debug, PartialEq, Clone)]
    pub struct State<T> {
        pub data: Box<RefCell<Option<T>>>,
    }
    /// State
    /// ```rust
    ///let a:State<i32> = State::new();    
    ///let b:State<i32> = State::new();    
    ///a.set_state(100);
    ///b.set_state(100);
    ///assert_eq!(a,b);
    ///&a.set_state(1000);
    ///assert_ne!(a.get_state(),b.get_state());
    /// ```
    impl<T> State<T> {
        pub fn new()->Self{
            use std::ptr::NonNull;
            let ptr = NonNull::<T>::dangling();
            Self { data: Box::new(RefCell::new(None)) }
        }

        pub fn set_state(&self, val: T) {
            self.data.swap(&RefCell::new(Some(val)));
        }
        pub fn get_state(&self) -> T {
            let state = self.clone().to_owned();
            let data = state.data.as_ptr();
            unsafe { data.read().expect("get some error") }
        }
    }
}
