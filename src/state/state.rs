#[allow(warnings)]
pub mod state {
    use core::fmt;
    use std::{cell::UnsafeCell, mem};
    pub struct State<T> {
        // Invariant: written to at most once.
        pub inner: UnsafeCell<Option<T>>,
    }

    impl<T> State<T> {
        /// Creates a new empty state.
        #[must_use]
        pub const fn new() -> State<T> {
            State {
                inner: UnsafeCell::new(None),
            }
        }

        /// Gets the reference to the underlying value.
        ///
        /// Returns `None` if the state is empty.
        pub fn get(&self) -> Option<&T> {
            // SAFETY: Safe due to `inner`'s invariant
            unsafe { &*self.inner.get() }.as_ref()
        }

        /// Gets the mutable reference to the underlying value.
        ///
        /// Returns `None` if the state is empty.
        pub fn get_mut(&mut self) -> Option<&mut T> {
            self.inner.get_mut().as_mut()
        }

        /// Sets the contents of the state to `value`.
        ///
        /// # Errors
        ///
        /// This method returns `Ok(())` if the state was seted 
        ///
        /// # Examples
        ///
        /// ```rust
        /// use zoon::State;
        /// let state = State::new();
        /// assert!(state.get().is_none());
        ///
        /// assert_eq!(state.set(92), Ok(()));
        /// assert_eq!(state.set(920), Ok(()));
        ///
        /// assert!(state.get().is_some());
        /// ```
        pub fn set(&self, value: T) -> Result<(), T> {
            // SAFETY: Safe because we cannot have overlapping mutable borrows
            let slot = unsafe { &*self.inner.get() };
            // if slot.is_some() {
            //     return Err(value);
            // }

            // SAFETY: This is the only place where we set the slot, no races
            // due to reentrancy/concurrency are possible, and we've
            // checked that slot is currently `None`, so this write
            // maintains the `inner`'s invariant.
            let slot = unsafe { &mut *self.inner.get() };
            *slot = Some(value);
            Ok(())
        }

        /// Gets the contents of the state, initializing it with `f`
        /// if the state was empty.
        ///
        /// # Panics
        ///
        /// If `f` panics, the panic is propagated to the caller, and the state
        /// remains uninitialized.
        ///
        /// It is an error to reentrantly initialize the state from `f`. Doing
        /// so results in a panic.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use zoon::State;
        /// let state = State::new();
        /// let value = state.get_or_init(|| 92);
        /// assert_eq!(value, &92);
        /// let value = state.get_or_init(|| unreachable!());
        /// assert_eq!(value, &92);
        /// ```
        pub fn get_or_init<F>(&self, f: F) -> &T
        where
            F: FnOnce() -> T,
        {
            match self.get_or_try_init(|| Ok::<T, !>(f())) {
                Ok(val) => val,
                Err(e) => e,
            }
        }

        /// Gets the contents of the state, initializing it with `f` if
        /// the state was empty. If the state was empty and `f` failed, an
        /// error is returned.
        ///
        /// # Panics
        ///
        /// If `f` panics, the panic is propagated to the caller, and the state
        /// remains uninitialized.
        ///
        /// It is an error to reentrantly initialize the state from `f`. Doing
        /// so results in a panic.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use zoon::State;
        /// let state = State::new();
        /// assert_eq!(state.get_or_try_init(|| Err(())), Err(()));
        /// assert!(state.get().is_none());
        /// let value = state.get_or_try_init(|| -> Result<i32, ()> {
        ///     Ok(92)
        /// });
        /// assert_eq!(value, Ok(&92));
        /// assert_eq!(state.get(), Some(&92))
        /// ```
        pub fn get_or_try_init<F, E>(&self, f: F) -> Result<&T, E>
        where
            F: FnOnce() -> Result<T, E>,
        {
            if let Some(val) = self.get() {
                return Ok(val);
            }
            /// Avoid inlining the initialization closure into the common path that fetches
            /// the already initialized value
            #[cold]
            fn outlined_call<F, T, E>(f: F) -> Result<T, E>
            where
                F: FnOnce() -> Result<T, E>,
            {
                f()
            }
            let val = outlined_call(f)?;
            // Note that *some* forms of reentrant initialization might lead to
            // UB (see `reentrant_init` test). I believe that just removing this
            // `assert`, while keeping `set/get` would be sound, but it seems
            // better to panic, rather than to silently use an old value.
            assert!(self.set(val).is_ok(), "reentrant init");
            Ok(self.get().unwrap())
        }

        /// Consumes the state, returning the wrapped value.
        ///
        /// Returns `None` if the state was empty.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use zoon::State;
        /// let state: State<String> = State::new();
        /// assert_eq!(state.into_inner(), None);
        ///
        /// let state = State::new();
        /// state.set("hello".to_string()).unwrap();
        /// assert_eq!(state.into_inner(), Some("hello".to_string()));
        /// ```
        pub fn into_inner(self) -> Option<T> {
            // Because `into_inner` takes `self` by value, the compiler statically verifies
            // that it is not currently borrowed. So it is safe to move out `Option<T>`.
            self.inner.into_inner()
        }

        /// Takes the value out of this `State`, moving it back to an uninitialized state.
        ///
        /// Has no effect and returns `None` if the `State` hasn't been initialized.
        ///
        /// Safety is guaranteed by requiring a mutable reference.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use zoon::State;
        /// let mut state: State<String> = State::new();
        /// assert_eq!(state.take(), None);
        ///
        /// let mut state = State::new();
        /// state.set("hello".to_string()).unwrap();
        /// assert_eq!(state.take(), Some("hello".to_string()));
        /// assert_eq!(state.get(), None);
        /// ```
        pub fn take(&mut self) -> Option<T> {
            mem::take(self).into_inner()
        }
    }
    impl<T> Default for State<T> {
        fn default() -> Self {
            Self::new()
        }
    }

    impl<T: fmt::Debug> fmt::Debug for State<T> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self.get() {
                Some(v) => f.debug_tuple("State").field(v).finish(),
                None => f.write_str("State(Uninit)"),
            }
        }
    }

    impl<T: Clone> Clone for State<T> {
        fn clone(&self) -> State<T> {
            let res = State::new();
            if let Some(value) = self.get() {
                match res.set(value.clone()) {
                    Ok(()) => (),
                    Err(_) => unreachable!(),
                }
            }
            res
        }
    }

    impl<T: PartialEq> PartialEq for State<T> {
        fn eq(&self, other: &Self) -> bool {
            self.get() == other.get()
        }
    }

    impl<T: Eq> Eq for State<T> {}

    impl<T> const From<T> for State<T> {
        /// Creates a new `State<T>` which already contains the given `value`.
        fn from(value: T) -> Self {
            State {
                inner: UnsafeCell::new(Some(value)),
            }
        }
    }
}
