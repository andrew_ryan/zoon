# State management

## Usage
Include state in your Cargo.toml [dependencies]:
```
[dependencies]
zoon = "0.1.6"
```
## get
 ```rust
 use zoon::State;
 let state = State::new();
 assert!(state.get().is_none());

 assert_eq!(state.set(92), Ok(()));

 assert!(state.get().is_some());
 ```
 ## get_or_init
 ```rust
 use zoon::State;
 let state = State::new();
 let value = state.get_or_init(|| 92);
 assert_eq!(value, &92);
 let value = state.get_or_init(|| unreachable!());
 assert_eq!(value, &92);
 ```
 ## get_or_try_init
 ```rust
 use zoon::State;
 let state = State::new();
 assert_eq!(state.get_or_try_init(|| Err(())), Err(()));
 assert!(state.get().is_none());
 let value = state.get_or_try_init(|| -> Result<i32, ()> {
     Ok(92)
 });
 assert_eq!(value, Ok(&92));
 assert_eq!(state.get(), Some(&92))
 ```

 ## into_inner
 ```rust
 use zoon::State;
 let state: State<String> = State::new();
 assert_eq!(state.into_inner(), None);

 let state = State::new();
 state.set("hello".to_string()).unwrap();
 assert_eq!(state.into_inner(), Some("hello".to_string()));
 ```

 ## take
 ```rust
 use zoon::State;
 let mut state: State<String> = State::new();
 assert_eq!(state.take(), None);

 let mut state = State::new();
 state.set("hello".to_string()).unwrap();
 assert_eq!(state.take(), Some("hello".to_string()));
 assert_eq!(state.get(), None);
 ```
